#!/Users/proteinmechanic/local/softwares/anaconda3/bin/python

import os,glob

###########################
list_ = glob.glob('*.q')
g_ = open('GESAMT.score','a')
for mem in list_:
	###########################
	# Superpose output parser
	###########################
	align_start = 0
	align_end = 0
	align_section = 0
	nl = 0
	q_score = -1
	align_len = -1
	rmsd = -1
	with open(mem,'r') as f_:
		for line in f_:
			line1 = line.rsplit()
			if len(line1) > 0 and line1[0] == 'Q-score':
				q_score = float(line1[2])
			if len(line1) > 0 and line1[0] == 'RMSD':
				rmsd = float(line1[2])
			if len(line1) > 0 and line1[0] == 'Aligned':
				align_len = line1[3]
	g_.write("\t".join(['#G#',mem.split('-')[0].split('.')[0],mem.split('-')[1].split('.')[0],str(round(q_score,4)),str(round(rmsd,4)),str(align_len)]) + '\n')
	os.system('rm ' + mem)
g_.close()

# The Structome Project

## Browser compatibility:

| OS | Version | Chrome | Firefox | Microsoft Edge | Safari |
| :--- | :----: | ---: | ---: | ---: | ---: |
| Linux | Ubuntu 18+ | not tested | not tested | not tested | not tested |
| MacOS | Big Sur | not tested | 108.0.1 | not tested | 15.0 |
| Windows | not tested | not tested | not tested | not tested | not tested |


## Method:

  - The sequences of proteins which had more than 50 amino acids were collected from the [_RCSB PDB_](https://www.rcsb.org/pages/download_features#FASTA). 
  - These sequences were then clustered at 90% sequence identity using [_usearch_](https://www.drive5.com/usearch/). Each cluster was then represented by its centroid. 
  - The structure representing the centroids of each cluster was obtained from  [_RCSB PDB_](https://www.rcsb.org/). 
  - All centroid structures were pairwise compared with all other centroids - both using their structure and sequences. The structure comparison metric [_GESAMT_](https://www.ccp4.ac.uk/html/gesamt.html) was used to compare protein structures and can found packaged in the [_CCP4 suite_](https://www.ccp4.ac.uk/) of programs. Protein sequences were compared using [_BLASTP_](https://blast.ncbi.nlm.nih.gov/Blast.cgi?CMD=Web&PAGE_TYPE=BlastDocs&DOC_TYPE=Download).
  - Annotations for each structure (where available) were obtained from [_SCOP_](http://scop.berkeley.edu/) and [_CATH_](http://www.cathdb.info/)

## User Interface:

### Structome

    http://structome.bii.a-star.edu.sg/

The user interface has four functional buttons for in-app navigation. 

  - Home; to navigate to the home-page from anywhere inside the app. 
  - Explore neighbours;  to see protein structures that are similar to the query protein (see details in the following section).
  - Explore clusters; to see other members of the cluster to which the query structure belongs to (see details in the following section).
  - Run locally; download a set of python scripts to compare protein structure and generate phylogenetic trees (see details in the following section). 

### Explore neighbours:

On the page, the user can query Structome using a protein structure. The input to this page is a combined PDB and chain ID search term. For example the term “1hv4_A”, queries Structome to look for chain A of protein with PDB ID 1hv4.   Pressing the submit button should return results for the search term. 

__Disclaimer__: While server load has been considered during the design of this app, note that the search term can take a few seconds, depending on concurrent usage load. The app may also time-out after an idle period. 
The results include a table listing matches with decreasing similarity (Q-score) and a neighbour-joining phylogenetic tree. Two additional buttons allow users to download the table data and the generated phylogenetic data. The phylogenetic data is made available in two formats a) as a distance matrix; distances are generated using 1 -  Q-score b) as a neighbour-joining tree string representing the tree shown on the “Explore neighbours” page. 

#### Neighbour statistics:

The rows in the table are interactive. Clicking a row in the table shows a detailed view of the selected result by showing the protein structure and sequence comparison results. 
The first table lists the same result row that the user clicked. Following this, the left of the screen then shows the result of a protein structure comparison between the user query structure’s cluster centroid (Red) and selected centroid (Blue). For the same proteins the BLASTP comparison result is shown on the right. The table lists the alignment parameters and to add more value, links to RCSB PDB are provided if domain assignments from SCOP, CATH or Pfam are needed. 

__Disclaimer__: The browser buttons cannot be used for in-app navigation. A “Return to results” button is provided to return the user to the results page. 

### Explore clusters:

On the page, the user can query Structome using a protein structure. The input to this page is a combined PDB and chain ID search term. For example the term “1hv4_A”, queries Structome to look for chain A of protein with PDB ID 1hv4. 
Pressing the submit button should return results for the search term. 
The results on this page return other proteins which are in the same cluster as the user query protein. The user query can be a centroid of a cluster or it can be just a member. The results returned list if the user query protein is only a member or a centroid. 
Structome comprises structure based comparisons only between centroid proteins. 
Rows in the table returned as result when querying a protein in “Explore clusters” are interactive. Clicking a row, shows a protein structure comparison between the protein structure in the selected row and the  centroid protein of that cluster.
A home button at the top allows users to navigate to the index page.  


### Run locally:

The shell and python scripts included allow users to generate their own distance data and network for analysis. See below for usage instructions.  

#### Script usage:

  - Create a folder
  - Place all scripts in this folder
  - Ensure:
      - Python is available in your current environment
      - Python has access to the numpy library
      - PDB files comprising protein structures are available in this folder
      - PDB files should have the naming convention “1hv4_A.pdb” where 1hv4 is the PDB ID and the particular file having this name only has chain A. 
      - There should ONLY be one protein chain per PDB file. Note: There is no internal check of the PDB file provided. The code assumes only a single chain is provided per PDB file. PDB files can be made to have a single chain, by simply deleting the remaining chains from the PDB file. A quick way to automate this is to use TCL atomselect commands in VMD. 
      - GESAMT is installed and available on the command line as “gesamt”. 
  - For any number of PDB files in the directory, running Master.sh should handle everything. This can simply be run on command line using 
      - sh Master.sh
  - If the run completes successfully, several files will be created. The file Network.Data has the taxa information and the distance matrix in nexus format, which can be loaded in phylogenetic analysis programs like SplitsTree4 to generate a neighbour-joining network or a neighbour-joining phylogenetic tree. 


#### Spot a bug:

Get in touch. Developed and maintained by Ashar Malik (asharjm@bii.a-star.edu.sg / ashar.malik@uq.edu.au)
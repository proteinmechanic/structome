#!/Users/proteinmechanic/local/softwares/anaconda3/bin/python
import os,glob,subprocess
import numpy as np

def MakePhyData(fl):

	d_0 = np.genfromtxt(fl,delimiter='\t',dtype=str,usecols=1,comments = "~")
	d_1 = np.genfromtxt(fl,delimiter='\t',dtype=str,usecols=2,comments = "~")
	d_2 = np.genfromtxt(fl,delimiter='\t',dtype=None,usecols=3,comments = "~")

	ud_0 = np.unique(d_0)
	mat = np.zeros(shape=[len(ud_0), len(ud_0)])

	for i in range(len(ud_0)):
		for j in range(len(ud_0)):
			if i > j:
				v1,v2 = '',''
				pat1 = ud_0[i] + '\t' + ud_0[j] + '\t'
				pat2 = ud_0[j] + '\t' + ud_0[i] + '\t'
				v1 = subprocess.Popen('/usr/local/bin/ggrep -P "' + pat1 + '" ' + fl,shell=True,stdout=subprocess.PIPE).stdout.read().decode().split('\t')[3]
				v2 = subprocess.Popen('/usr/local/bin/ggrep -P "' + pat2 + '" ' + fl,shell=True,stdout=subprocess.PIPE).stdout.read().decode().split('\t')[3]

				_A_ = ""
				
				# Handle failed comparisons 
				if v1 == "-1":
					# change this to a "0" indicating no similarity so later it becomes 1 = max distance
					v1 = "0.0"
				if v2 == "-1":
					v2 = "0.0"

				# Consider all cases that need attention before attempting to build symmetrical matrix
				val = -999
				v1 = float(v1)
				v2 = float(v2)
				if v1 == 0.0 and v2 != 0.0:
					val = v2
				elif v1 != 0.0 and v2 == 0.0:
					val = v1
				elif v1 != 0.0 and v2 != 0.0:
					val = (v1 + v2)/2
				elif v1 == 0.0 and v2 == 0.0:
					val = 0.0
				_A_ = round(1 - val,5)

				mat[i,j] = _A_
				mat[j,i] = _A_
	return ud_0,mat

fl = 'GESAMT.score'
tax, mat = MakePhyData(fl)

for i in tax:
	with open('taxas.txt','a') as f:
		f.write(i + '\n')
		
f = open('distance_.log','a')
for r_ in range(0,len(tax)):
	for c_ in range(0,len(tax)):
		f.write('{:8.4f}'.format(mat[r_][c_]))
	f.write('\n')
f.close()
